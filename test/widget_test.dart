import 'package:appforsanta/UI/child_list/child_list_cubit.dart';
import 'package:appforsanta/models/child_data.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:test/test.dart';


void main() {
  group('ChildListCubit', () {
    blocTest<ChildListCubit, ChildListState>(
      'emits a new state with the new child added',
      build: () => ChildListCubit(), // create a new instance here
      act: (cubit) => cubit.addChild(ChildData(id: 1, name: 'John', country: 'USA', isNice: true)),
      expect: () => [
        ChildListState(childList: [ChildData(id: 1, name: 'John', country: 'USA', isNice: true)]),
      ],
    );
  });
}
