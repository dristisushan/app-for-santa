import 'package:equatable/equatable.dart';

class ChildData extends Equatable{
  int id;
  String name;
  String country;
  bool isNice;

  ChildData({required this.id,required this.name, required this.country, this.isNice = true});

  @override
  List<Object> get props => [id, name, country, isNice];

}