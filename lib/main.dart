import 'package:flutter/material.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'UI/child_list/child_list_cubit.dart';
import 'UI/child_list/child_list_page.dart';
// Entry point of app
void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ChildListCubit(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Christmas List',
        home: ChildListScreen(),
      ),
    );
  }
}



