

import 'package:appforsanta/UI/child_list/child_list_cubit.dart';
import 'package:appforsanta/models/child_data.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DialogBox {

  static void showAddChildDialog(BuildContext context, int id) {
    final formKey = GlobalKey<FormState>();
    String name = '';
    String country = '';
    bool isNice = true;

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Add New Child'),
          content: StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
              return Form(
                key: formKey,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    TextFormField(
                      decoration: const InputDecoration(labelText: 'Name'),
                      onSaved: (value) => name = value ?? '',
                      validator: (value) => value!.isEmpty ? 'Please enter a name' : null,
                    ),
                    TextFormField(
                      decoration: const InputDecoration(labelText: 'Country'),
                      onSaved: (value) => country = value ?? '',
                      validator: (value) => value!.isEmpty ? 'Please enter a country' : null,
                    ),
                    SwitchListTile(
                      title: Text(isNice?'Nice': "Naughty"),
                      value: isNice,
                      contentPadding: EdgeInsets.zero,
                      onChanged: (bool value) {
                        setState(() {
                          isNice = value;
                        });
                      },
                    ),
                  ],
                ),
              );
            },
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: const Text('Add'),
              onPressed: () {
                if (formKey.currentState!.validate()) {
                  formKey.currentState!.save();
                  final newChild = ChildData(id: id, name: name, country: country, isNice: isNice);
                  context.read<ChildListCubit>().addChild(newChild);
                  Navigator.of(context).pop();
                }
              },
            ),
          ],
        );
      },
    );
  }

}