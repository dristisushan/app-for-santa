import 'package:appforsanta/UI/widget/dialog_box/dialog_box.dart';
import 'package:appforsanta/models/child_data.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'child_list_cubit.dart';

class ChildListScreen extends StatelessWidget {
  const ChildListScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Christmas List')),
      body: BlocBuilder<ChildListCubit, ChildListState>(
        builder: (context, state) {
          return ListView.builder(
            // list child with country and status
            itemCount: state.childList.length,
            itemBuilder: (context, index) {
              final child = state.childList[index];
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: ListTile(
                    title: Text(
                      child.name,
                      style: const TextStyle(
                          fontSize: 18,
                          color: Colors.green,
                          fontWeight: FontWeight.bold),
                    ),
                    subtitle: Text(
                      'From: ${child.country}   -   Status: ${child.isNice ? 'Nice' : 'Naughty'}',
                      style: const TextStyle(fontSize: 16),
                    ),
                    shape: const RoundedRectangleBorder(
                        side: BorderSide(
                            color: Color(0xFF2A8068)), //the outline color
                        borderRadius: BorderRadius.all(Radius.circular(4))),
                    onTap: () {
                      // to update children behaviour
                      final updatedChild = ChildData(
                          id: child.id,
                          name: child.name,
                          country: child.country,
                          isNice: !child.isNice);
                      context.read<ChildListCubit>().updateChild(updatedChild);
                    }),
              );
            },
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          // add new child to list
          DialogBox.showAddChildDialog(context,
              context.read<ChildListCubit>().state.childList.length + 1);
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
