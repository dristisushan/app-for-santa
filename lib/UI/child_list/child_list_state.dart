part of 'child_list_cubit.dart';

class ChildListState extends Equatable {
  final List<ChildData> childList;

  const ChildListState({
    this.childList = const [],
  });

  @override
  List<Object?> get props => [
        childList,
      ];

  ChildListState copyWith({
    List<ChildData>? childList,
  }) {
    return ChildListState(
      childList: childList ?? this.childList,
    );
  }
}
