import 'package:appforsanta/models/child_data.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'child_list_state.dart';



  class ChildListCubit extends Cubit<ChildListState> {
  ChildListCubit() : super(const ChildListState());

  void addChild(ChildData child) {
    final updatedList = List<ChildData>.from(state.childList)..add(child);
    emit(state.copyWith(childList: updatedList));
  }

  void updateChild(ChildData child) {
    final updatedList = state.childList.map((c) {
      return c.id == child.id ? child : c;
    }).toList();
    emit(state.copyWith(childList: updatedList));
  }
  }

